package mpr.db.repositories;

import mpr.db.PagingInfo;
import mpr.db.RolePermissionRepository;
import mpr.db.unitofwork.IUnitOfWorkRepository;
import mpr.domain.Entity;
import mpr.domain.EntityState;
import mpr.domain.RolePermission;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqLiteRolePermissionRepository implements RolePermissionRepository, IUnitOfWorkRepository {

    private Connection connection;

    private String insertSql = "INSERT INTO RolePermission (roleId, permissionId) VALUES (?, ?)";
    private String selectSql = "SELECT * FROM RolePermission LIMIT ?, ?";
    private String selectByIdSql ="SELECT * FROM RolePermission WHERE id = ?";
    private String selectByRoleIdSql ="SELECT * FROM RolePermission WHERE roleId = ? LIMIT ?, ?";
    private String selectByPermissionIdSql ="SELECT * FROM RolePermission WHERE permissionId = ? LIMIT ?, ?";
    private String deleteSql = "DELETE FROM RolePermission WHERE id = ?";
    private String updateSql = "UPDATE RolePermission SET roleId = ?, permissionId = ? WHERE id = ?";


    private PreparedStatement insert;
    private PreparedStatement select;
    private PreparedStatement selectById;
    private PreparedStatement selectByRoleId;
    private PreparedStatement selectByPermissionId;
    private PreparedStatement delete;
    private PreparedStatement update;

    private String createTableRolePermission = "CREATE TABLE RolePermission(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "roleId INT," +
            "permissionId INT)";

    public SqLiteRolePermissionRepository(Connection connection) {
        this.connection = connection;
        try {
            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while(rs.next())
            {
                if(rs.getString("TABLE_NAME").equalsIgnoreCase("RolePermission")){
                    tableExists=true;
                    break;
                }
            }
            if(!tableExists){
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(createTableRolePermission);
            }

            insert = connection.prepareStatement(insertSql);
            select = connection.prepareStatement(selectSql);
            selectById = connection.prepareStatement(selectByIdSql);
            selectByRoleId = connection.prepareStatement(selectByRoleIdSql);
            selectByPermissionId = connection.prepareStatement(selectByPermissionIdSql);
            delete = connection.prepareStatement(deleteSql);
            update = connection.prepareStatement(updateSql);
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    public List<RolePermission> withRoleId(Integer roleId, PagingInfo page) {
        List<RolePermission> result = new ArrayList<RolePermission>();
        try {
            selectByRoleId.setInt(1, roleId);
            selectByRoleId.setInt(2, page.getCurrentPage() * page.getSize());
            selectByRoleId.setInt(3, page.getSize());
            ResultSet rs = selectByRoleId.executeQuery();
            while(rs.next()){
                RolePermission rolePermission = new RolePermission();
                rolePermission.setRoleId(rs.getInt("roleId"));
                rolePermission.setPermissionId(rs.getInt("permissionId"));
                rolePermission.setId(rs.getInt("id"));
                result.add(rolePermission);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<RolePermission> withPermissionId(Integer permissionId, PagingInfo page) {
        List<RolePermission> result = new ArrayList<RolePermission>();
        try {
            selectByPermissionId.setInt(1, permissionId);
            selectByPermissionId.setInt(2, page.getCurrentPage() * page.getSize());
            selectByPermissionId.setInt(3, page.getSize());
            ResultSet rs = selectByPermissionId.executeQuery();
            while(rs.next()){
                RolePermission rolePermission = new RolePermission();
                rolePermission.setRoleId(rs.getInt("roleId"));
                rolePermission.setPermissionId(rs.getInt("permissionId"));
                rolePermission.setId(rs.getInt("id"));
                result.add(rolePermission);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public RolePermission withId(int id) {
        RolePermission result = null;
        try {
            selectById.setInt(1, id);
            ResultSet rs = selectById.executeQuery();
            while(rs.next()){
                RolePermission rolePermission = new RolePermission();
                rolePermission.setRoleId(rs.getInt("roleId"));
                rolePermission.setPermissionId(rs.getInt("permissionId"));
                rolePermission.setId(rs.getInt("id"));
                result = rolePermission;
                break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<RolePermission> allOnPage(PagingInfo page) {
        List<RolePermission> result = new ArrayList<RolePermission>();

        try {
            select.setInt(1, page.getCurrentPage() * page.getSize());
            select.setInt(2, page.getSize());
            ResultSet rs = select.executeQuery();
            while(rs.next()){
                RolePermission rolePermission = new RolePermission();
                rolePermission.setRoleId(rs.getInt("roleId"));
                rolePermission.setPermissionId(rs.getInt("permissionId"));
                rolePermission.setId(rs.getInt("id"));
                result.add(rolePermission);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Integer add(RolePermission rolePermission) {
        Integer id = 0;
        try {
            insert.setInt(1, rolePermission.getRoleId());
            insert.setInt(2, rolePermission.getPermissionId());
            insert.executeUpdate();
            ResultSet rs = insert.getGeneratedKeys();
            if (rs.next()){
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void modify(RolePermission rolePermission) {
        try {
            update.setInt(1, rolePermission.getRoleId());
            update.setInt(2, rolePermission.getPermissionId());
            update.setInt(3, rolePermission.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void remove(RolePermission rolePermission) {
        try {
            delete.setInt(1, rolePermission.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void persistAdd(Entity entity) {
        if(entity.getState() == EntityState.New){
            add((RolePermission) entity);
        }
    }

    public void persistUpdate(Entity entity) {
        if(entity.getState() == EntityState.Changed){
            modify((RolePermission) entity);
        }
    }

    public void persistDelete(Entity entity) {
        if(entity.getState() == EntityState.Deleted){
            remove((RolePermission) entity);
        }
    }

}