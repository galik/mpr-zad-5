package mpr.db;

import mpr.domain.Person;

import java.util.List;

/**
 * Created by galik on 21.01.2016.
 */
public interface PersonRepository extends Repository<Person> {

    List<Person> withFirstName(String firstName, PagingInfo page);
    List<Person> withSurname(String surname, PagingInfo page);
    List<Person> withAddressId(Integer addressId, PagingInfo page);
}
