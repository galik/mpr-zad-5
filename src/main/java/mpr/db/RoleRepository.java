package mpr.db;

import mpr.domain.Role;

import java.util.List;

/**
 * Created by galik on 21.01.2016.
 */
public interface RoleRepository extends Repository<Role> {

    List<Role> withRoleName(String roleName, PagingInfo page);
}
