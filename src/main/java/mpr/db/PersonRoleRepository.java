package mpr.db;

import mpr.domain.PersonRole;

import java.util.List;

/**
 * Created by galik on 21.01.2016.
 */
public interface PersonRoleRepository extends Repository<PersonRole> {

    List<PersonRole> withPersonId(Integer personId, PagingInfo page);
    List<PersonRole> withRoleId(Integer roleId, PagingInfo page);
}
