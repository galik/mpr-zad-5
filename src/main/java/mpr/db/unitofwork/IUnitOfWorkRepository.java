package mpr.db.unitofwork;

import mpr.domain.Entity;

/**
 * Created by galik on 22.01.2016.
 */
public interface IUnitOfWorkRepository {

    public void persistAdd(Entity entity);
    public void persistUpdate(Entity entity);
    public void persistDelete(Entity entity);
}
