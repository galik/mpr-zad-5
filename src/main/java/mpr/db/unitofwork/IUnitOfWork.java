package mpr.db.unitofwork;

import mpr.domain.Entity;

/**
 * Created by galik on 22.01.2016.
 */
public interface IUnitOfWork {

    public void commit();
    public void rollback();
    public void markAsNew(Entity entity, IUnitOfWorkRepository repository);
    public void markAsDirty(Entity entity, IUnitOfWorkRepository repository);
    public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository);
}
