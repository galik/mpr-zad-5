package mpr.tests;

import mpr.domain.Address;
import mpr.domain.Permission;
import mpr.domain.Person;
import mpr.domain.Role;
import mpr.service.PersonService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Created by galik on 31.01.2016.
 */
public class PersonServiceTest {

    List<Person> persons = new ArrayList<>();
    Person person1 = new Person();
    Person person2 = new Person();
    Person person3 = new Person();
    Role role1 = new Role();
    Role role2 = new Role();
    Permission perm1 = new Permission();
    Permission perm2 = new Permission();

    @Before
    public void initMocks() {
        person1.setFirstName("Sone");
        person1.setSurname("ONEONEONE");
        person1.setAge(30);

        person2.setFirstName("Two");
        person2.setSurname("TWOTWO");
        person2.setAge(25);

        person3.setFirstName("Sthree");
        person3.setSurname("THREE");
        person3.setAge(16);

        role1.setId(1);
        role1.setRoleName("role1");

        role2.setId(2);
        role2.setRoleName("role2");

        perm1.setId(1);
        perm1.setPermissionName("Permission1");
        perm1.setPermissionLevel(1);

        perm2.setId(2);
        perm2.setPermissionName("Permission2");
        perm2.setPermissionLevel(22);

        role1.addRoleSet(perm1);
        role2.addRoleSet(perm2);

        person1.addRoles(role1);
        person2.addRoles(role1);
        person2.addRoles(role2);
        person3.addRoles(role2);

        Address adr1 = new Address();
        Address adr2 = new Address();
        adr1.setId(1);
        adr2.setId(2);

        person1.addAdress(adr1);
        person2.addAdress(adr2);
        person3.addAdress(adr2);

        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
    }

    @Test
    public void testFindPersonsWhoHaveMoreThanOneRole() {
        List<Person> personsWithRoles;
        personsWithRoles = PersonService.findPersonsWhoHaveMoreThanOneRole(persons);
        assertSame(personsWithRoles.get(0), person2);
    }

    @Test
    public void TestFindOldestPerson() {
        assertSame(PersonService.findOldestPerson(persons), person1);

    }

    @Test
    public void TestFindPersonWithLongestSurname() {
        assertSame(PersonService.findPersonWithLongestSurname(persons), person1);

    }

    @Test
    public void TestGetNamesAndSurnamesCommaSeparatedOfAllPersonsAbove18() {
        String namesWithSurnames = person1.getFirstName()+" "+person1.getSurname()
                +","+person2.getFirstName()+" "+person2.getSurname();
        assertEquals(PersonService.getNamesAndSurnamesCommaSeparatedOfAllPersonsAbove18(persons), namesWithSurnames);

    }

    @Test
    public void TestGetSortedDescRoleOfPersonsWithNameStartingWithS() {
        List<String> roles = new ArrayList<>();
        roles.add(role2.getRoleName());
        roles.add(role1.getRoleName());
        assertEquals(PersonService.getSortedDescRoleOfPersonsWithNameStartingWithS(persons), roles);
    }

    @Test
    public void TestGroupPersonsByAddress() {
        Map<Integer, List<Person>> groupUsersByAddress = new HashMap<Integer, List<Person>>();
        List<Person> personsWithAddress1 = new ArrayList<>();
        List<Person> personsWithAddress2 = new ArrayList<>();

        personsWithAddress1.add(person1);
        personsWithAddress2.add(person2);
        personsWithAddress2.add(person3);
        groupUsersByAddress.put(1, personsWithAddress1);
        groupUsersByAddress.put(2, personsWithAddress2);
        assertEquals(PersonService.groupPersonsByAddress(persons), groupUsersByAddress);
    }

    @Test
    public void TestPartitionPersonByUnderAndOver18() {
        Map<Boolean, List<Person>> partitionUsersByAge = new HashMap<Boolean, List<Person>>();
        List<Person> personsAbove18 = new ArrayList<>();
        List<Person> personsBelow18 = new ArrayList<>();

        personsAbove18.add(person1);
        personsAbove18.add(person2);
        personsBelow18.add(person3);
        partitionUsersByAge.put(true, personsAbove18);
        partitionUsersByAge.put(false, personsBelow18);

        assertEquals(PersonService.partitionPersonByUnderAndOver18(persons), partitionUsersByAge);
    }
}
