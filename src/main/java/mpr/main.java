package mpr;

import mpr.db.PagingInfo;
import mpr.db.PersonRepository;
import mpr.db.RepositoryCatalog;
import mpr.db.catalogs.SqLiteRepositoryCatalog;
import mpr.db.repositories.SqLitePersonRepository;
import mpr.db.repositories.SqLitePersonRoleRepository;
import mpr.db.repositories.SqLiteRoleRepository;
import mpr.db.unitofwork.UnitOfWork;
import mpr.domain.*;
import mpr.service.PersonService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by galik on 21.01.2016.
 */
public class main {

    public static void main(String[] args) {

        String dbUrl = "jdbc:sqlite:mpr.db";
        Connection connection;

        try {
            connection = DriverManager.getConnection(dbUrl);
            RepositoryCatalog catalogOf = new SqLiteRepositoryCatalog(connection);

            PersonService ps = new PersonService();
            UnitOfWork uow = new UnitOfWork(connection);

//            Person jan = new Person();
//            Person marcin = new Person();
//            Person zbyszek = new Person();
//            Person stanislaw = new Person();
//
//            jan.setFirstName("Sławek");
//            jan.setSurname("Kowalski");
//            jan.setAge(15);
//            marcin.setFirstName("Marcin");
//            marcin.setSurname("Iksiński");
//            marcin.setAge(28);
//            zbyszek.setFirstName("Stefan");
//            zbyszek.setSurname("Nowak");
//            zbyszek.setAge(31);
//            stanislaw.setFirstName("Stanisław");
//            stanislaw.setSurname("Wołoszański");
//            stanislaw.setAge(75);
//
//            uow.markAsNew(jan, new SqLitePersonRepository(connection));
//            uow.markAsNew(marcin, new SqLitePersonRepository(connection));
//            uow.markAsNew(zbyszek, new SqLitePersonRepository(connection));
//            uow.markAsNew(stanislaw, new SqLitePersonRepository(connection));
//            uow.commit();

//            Role r = new Role();
//            Role rr = new Role();
//            r.setRoleName("Rola2");
//            rr.setRoleName("Rola3");
//            uow.markAsNew(r, new SqLiteRoleRepository(connection));
//            uow.markAsNew(rr, new SqLiteRoleRepository(connection));
//            PersonRole pr = new PersonRole();
//            PersonRole pr2 = new PersonRole();
//            pr.setPersonId(4);
//            pr.setRoleId(2);
//            pr2.setPersonId(3);
//            pr2.setRoleId(3);
//            uow.markAsNew(pr, new SqLitePersonRoleRepository(connection));
//            uow.markAsNew(pr2, new SqLitePersonRoleRepository(connection));
//            uow.commit();

            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.setSize(10);
            List<Person> persons = catalogOf.person().allOnPage(pagingInfo);
            List<Person> personsWithRoles;
//            System.out.println(persons.isEmpty());
            System.out.println("*** Person with more than 1 Role:");
            personsWithRoles = PersonService.findPersonsWhoHaveMoreThanOneRole(persons);
            for (Person p : personsWithRoles) {
                System.out.println(p.getPersonDetails());
            }
            System.out.println("*** Oldest Person : "+PersonService.findOldestPerson(persons).getPersonDetails());
            System.out.println("*** Person with longest surname : "+PersonService.findPersonWithLongestSurname(persons).getPersonDetails());
            System.out.println("*** Persons above 18 : "+PersonService.getNamesAndSurnamesCommaSeparatedOfAllPersonsAbove18(persons));
            System.out.println("*** Sorted Roles of Users with Names starting with S (desc): "+PersonService.getSortedDescRoleOfPersonsWithNameStartingWithS(persons));
            System.out.println("*** Very weird function output:");
            PersonService.printLowerCasedRoleNamesOfPersonsWithSurnameEndingWithI(persons);

            Map<Integer, List<Person>> groupUsersByAdress = PersonService.groupPersonsByAddress(persons);
            System.out.println(groupUsersByAdress);

            Map<Boolean, List<Person>> groupUsersBy18 = PersonService.partitionPersonByUnderAndOver18(persons);
            System.out.println(groupUsersBy18);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
