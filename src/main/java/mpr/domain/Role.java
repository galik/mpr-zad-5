package mpr.domain;

import java.util.*;

/**
 *
 * @author s13128
 */
public class Role extends Entity {

    private String roleName;
    private List<Permission> permissionSet = new ArrayList<Permission>();

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * @return mpr.domain.Permission
     */
    public List<Permission> getPermissionSet() {
        return permissionSet;
    }

    /**
     * @param permissionSet
     */
    public void setRoleSet(List<Permission> permissions) {
        this.permissionSet = permissions;
    }

    /**
     * @param permissionSet
     */
    public void addRoleSet(Permission permission) {
        this.permissionSet.add(permission);
    }

}
