package mpr.domain;

/**
 * Created by galik on 22.01.2016.
 */
public enum  EntityState {
    New, Changed, Unchanged, Deleted;
}
