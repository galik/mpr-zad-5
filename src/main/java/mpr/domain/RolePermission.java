package mpr.domain;

/**
 *
 * @author s13128
 */
public class RolePermission extends Entity {

    private Integer roleId;
    private Integer permissionId;

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}
