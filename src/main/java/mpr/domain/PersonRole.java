package mpr.domain;

/**
 *
 * @author s13128
 */
public class PersonRole extends Entity {

    private Integer personId;
    private Integer roleId;

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}
