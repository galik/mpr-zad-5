package mpr.domain;

/**
 * Created by galik on 22.01.2016.
 */
public abstract class Entity {

    private int id;

    EntityState state;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

}
